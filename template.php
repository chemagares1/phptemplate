<?php
	//Variable de la página en la que estamos
      $page = htmlspecialchars($_GET["page"]);

      //Ruta de $page
	$phpRoute = "pages/".$page.".php";

      //Ruta archivo js de $page
      $jsRoute = "js/".$page.".js";

      //Ruta archivo css de $page
      $cssRoute = "css/".$page.".css";


      //Uncomment to test files routes:
      //echo 'Página actual: '.$page.'<br/>';
      //echo 'Ruta de la página: '.$phpRoute.'<br/>';
      //echo 'Ruta del css: '.$cssRoute.'<br/>';
      //echo 'Ruta del js: '.$jsRoute.'<br/>';
?>

<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <META name="robots" content="INDEX,FOLLOW">
    <title>Basic PHP Template System</title>
    <?php echo '<link rel="stylesheet" type="text/css" href="'.$cssRoute.'">'; ?> <!--Current page CSS load-->
  </head>
  <body>
	
	<?php require_once("menus/menu.php"); ?> <!--Static HEADER-->

	
	<?php require_once($phpRoute); ?> <!--Current page load-->

	
	<?php require_once("menus/footer.php"); ?> <!--Static FOOTER-->
	
	<?php echo '<script type="text/javascript" src="'.$jsRoute.'"></script>'; ?> <!--Current page JS load-->
  </body>
</html>